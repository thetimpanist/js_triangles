var canvasElement = document.querySelector("#main_canvas");
var context = canvasElement.getContext("2d");
var answer = document.querySelector("#answer");
var correctCount = document.querySelector("#correct");
var incorrectCount = document.querySelector("#wrong");


const checkAnswer = function(input){
  if(input == answer.value){
    updateCount(correctCount);
  }
  else{
    updateCount(incorrectCount);
    alert("Oh REALLY?????");
  }
  run();
}

const updateCount = function(counter){
  counter.innerHTML = parseInt(counter.innerHTML) + 1;
}

const lineWidth = 10;

const clearCanvas = function(){
  context.clearRect(0, 0, canvasElement.width, canvasElement.height);
  answer.value = 'nonright';
}

const drawTriangle = function(points) {
  // the triangle
  context.beginPath();
  context.moveTo(points[0][0], points[0][1]);
  context.lineTo(points[1][0], points[1][1]);
  context.lineTo(points[2][0], points[2][1]);
  context.closePath();

  // the outline
  context.lineWidth = lineWidth;
  context.strokeStyle = '#666666';
  context.stroke();

  // the fill color
  context.fillStyle = "#FFCC00";
  context.fill();
}

const randomPoint = function(){
  return [Math.floor(Math.random() * 500), Math.floor(Math.random() * 500)]
}

const lineLength = function(pointA, pointB){
  return Math.sqrt(
    Math.pow(pointB[0] - pointA[0], 2) + Math.pow(pointB[1] - pointA[1], 2)
  )
}

const angle = function(a, b, c){
  return Math.acos(
    (Math.pow(a, 2) + Math.pow(b, 2) - Math.pow(c, 2)) / (2 * a * b)
  ) * 180 / Math.PI;
}

const getPivotPoint = function(angles){
  for(i = 0; i < angles.length; i++){
    if(angles[i] > 80 && angles[i] < 100){
      return i;
    }
  }
  return 0;
}

const getX = function(point, slope, length){
  diff = length * Math.sqrt(1 / (1 + Math.pow(slope, 2)))
  return [point[0] + diff, point[0] - diff];
}
const getY = function(point, slope, length){
  diff = slope * length * Math.sqrt(1 / (1 + Math.pow(slope, 2)))
  return [point[1] + diff, point[1] - diff];
}

const getClosest = function(point, pos, neg){
  diffPos = Math.abs(point[0] - pos[0] + point[1] - pos[1]);
  diffNeg = Math.abs(point[0] - neg[0] + point[1] - neg[1]);
  if(diffPos > diffNeg){
    return neg;
  }
  return pos;
}

const makeRight = function(points, lines, angles){
  pivot = getPivotPoint(angles);
  slope = getSlope(points[pivot], points[(pivot + 1) % 3]);
  perpSlope = -1 / slope;

  x = getX(points[pivot], perpSlope, lines[(pivot + 2) % 3]),
  y = getY(points[pivot], perpSlope, lines[(pivot + 2) % 3])
  pos = [x[0], y[0]];
  neg = [x[1], y[1]];

  points[(pivot + 2) % 3] = getClosest(points[(pivot + 2) % 3], pos, neg);
  return points;
}

const getSlope = function(a, b){
  return (a[1] - b[1]) / (a[0] - b[0]);
}

const getDimensions = function(points){
  lines = [
    lineLength(points[0], points[1]),
    lineLength(points[1], points[2]),
    lineLength(points[2], points[0]),
  ]

  angles = [
    angle(lines[2], lines[0], lines[1]),
    angle(lines[0], lines[1], lines[2]),
    angle(lines[1], lines[2], lines[0])
  ]
  return [lines, angles];
}

const run = function(){
  clearCanvas();
  angles = [0,0,0];
  while(!(angles.every((a) => a > lineWidth + 2))){
    points = [randomPoint(), randomPoint(), randomPoint()];
    dims = getDimensions(points);
    lines = dims[0];
    angles = dims[1];
  }


  if(!(angles.every((a) => a < 80 || a > 100))){
    points = makeRight(points, lines, angles);
    dims = getDimensions(points);
    lines = dims[0];
    angles = dims[1];
    answer.value = 'right';
  }

  console.log(points);
  console.log(lines)
  console.log(angles);

  drawTriangle(points);
};
run();
